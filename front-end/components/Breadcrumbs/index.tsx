import { Breadcrumbs as BreadcrumbsMUI, Link } from "@material-ui/core";
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { useRouter } from "next/router";
import styles from '../../styles/components/Breadcrumbs.module.css'

export default function Breadcrumbs() {
  const { asPath } = useRouter()
  const links = asPath.split('/').filter((value) => value)
  
  return <div className={styles.container}>
    <BreadcrumbsMUI separator={<NavigateNextIcon fontSize="small" />} aria-label="breadcrumb">
      {links.map(link => <Link href="/" key={link} >
        <span className={styles.link}>
          {link}
        </span>
      </Link>)}
    </BreadcrumbsMUI>
  </div>
}
