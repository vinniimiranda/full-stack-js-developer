import { Link as LinkMUI } from '@material-ui/core'
import Link from 'next/link'
import { useRouter } from 'next/router'
import styles from '../../styles/components/Navbar.module.css'

interface Route {
  label: string;
  href: string;
  subMenu?: Route[]
}

export default function Navbar() {

  const routes: Route[] = [
    { label: 'Shop', href: '/' },
    {
      label: 'Recipes', href: '/recipes', subMenu: [
        {
          label: 'Categories',
          href: '/recipes/categories'
        },
        {
          label: 'Collections',
          href: '/recipes/collections'
        },
        {
          label: 'Resources',
          href: '/recipes/resources'
        },
      ]
    },
    { label: 'Learn', href: '/learn' },
    { label: 'About', href: '/about' },
  ]

  const { route } = useRouter()
  const [, currentRoute] = route.split('/')
  const { subMenu: subItems = [] } = routes.find(({ href }) => currentRoute === href.replace('/', ''))


  return <>
    <ul className={styles.navBarContainer}>
      {routes.map(({ label, href }) => <Link key={href} href={href}>
        <span className={styles.navItem}>
          {label}
          {href.replace('/', '') === currentRoute && <div className={styles.activeItem}></div>}
        </span>
      </Link>)}

    </ul>
    {subItems.length ? <ul className={styles.subMenuContainer}>
      {subItems.map((item) => <li className={styles.subMenuItem} key={item.href} >{item.label}</li>)}
    </ul>
      :
      <></>
    }
  </>


}
