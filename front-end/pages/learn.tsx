import { Box, Typography } from "@material-ui/core";

export default function Learn() {
  return <Box padding="2rem"> 
    <Typography variant="h4">Learn</Typography>
    <Typography variant="body2">Consequat voluptate in velit nisi. Do pariatur cillum mollit exercitation et. Officia eiusmod adipisicing dolore esse.

    Nulla cupidatat nulla reprehenderit proident voluptate consectetur nulla ipsum pariatur adipisicing commodo exercitation. Elit Lorem non consectetur ut amet nisi et aute est tempor ex duis do. Non aliquip eiusmod culpa ad ea veniam elit do consectetur eiusmod. Incididunt magna do voluptate reprehenderit ut incididunt id. Proident sint proident elit aliquip fugiat. Ea laboris mollit sunt labore consequat aliquip non mollit minim. Ea et ullamco officia laborum cillum quis ad non cupidatat non veniam.

    Sunt id anim aute nulla consequat Lorem. Est nulla dolor ex consectetur excepteur incididunt nulla laboris commodo qui fugiat non cillum do. Voluptate aliqua deserunt nostrud ullamco qui culpa laboris exercitation. Elit veniam velit officia ea ullamco qui commodo consequat voluptate pariatur reprehenderit culpa officia.

    Laborum elit nisi aliquip nulla sit mollit laborum enim ex aliqua fugiat aute. Excepteur mollit ut exercitation ut elit sit excepteur dolor. Id pariatur non officia et ex. Ullamco proident dolor amet laborum id officia deserunt minim. Ea magna ad pariatur esse ullamco ad. Reprehenderit esse dolore non occaecat aute id pariatur non in enim do sunt exercitation.

Et ipsum nostrud ipsum ipsum veniam. Pariatur cupidatat enim do ad consectetur ullamco dolor voluptate enim proident non mollit duis. Exercitation pariatur voluptate do qui irure ea pariatur velit qui sint id. Adipisicing dolore incididunt ipsum do qui. In do aliquip adipisicing nostrud esse excepteur sunt. Id velit mollit eu sint adipisicing incididunt anim. Eu consectetur cupidatat ex ad nisi eu et deserunt dolore Lorem id laborum dolore magna. </Typography>
  </Box>
}
