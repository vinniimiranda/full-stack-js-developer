
import { Box, Card, CardActionArea, CardContent, CardMedia, Grid, Typography } from '@material-ui/core';
import Link from 'next/link';
import categories from '../../mocks/categories';
import styles from '../../styles/Recipes.module.css'

export default function Recipes() {

  return <Box flex={1} padding="3rem" width="100%">
    <Grid container spacing={2} style={{flexGrow: 1}}>
      {categories.map(({ name, link, image }) => <Grid item key={name} xs={12} md={4}>
        <Link href={link}>
          <Card >
            <CardActionArea>
              <CardMedia
                className={styles.cardImage}
                image={image}
                title={name}
              />
              <CardContent>
                <Typography className={styles.categoryTitle} gutterBottom variant="h5" component="h2">
                  {name}
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Link>
      </Grid>)}
    </Grid>
  </Box>
}
