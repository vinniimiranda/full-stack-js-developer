
import { Box, Button, Divider, Grid, Typography } from '@material-ui/core';
import { Save, Print } from '@material-ui/icons';

import { useRouter } from 'next/router';
import categories from '../../mocks/categories';
import styles from '../../styles/RecipeCategory.module.css'

export default function RecipeCategory() {
  const router = useRouter();
  const path = router.asPath || '';

  const [, , category] = path.split('/');

  const recipe = categories.find(({ name }) => name === category)?.recipes[0]

  return (
    <Box className={styles.container}>
      <Box className={styles.info}>
        <Typography variant="h3" >{recipe?.title}</Typography>
        <Typography className={styles.description} variant="body1">{recipe?.description}</Typography>
        <Box>
          <Grid container spacing={3}>
            <Grid item>
              <Box>
                <span className={styles.times}>PREP</span>
                <Typography>{recipe?.prepTime} minutes</Typography>
              </Box>
            </Grid>
            <Grid item>
              <Box>
                <span className={styles.times}>BAKE</span>
                <Typography>{recipe?.bakeTime} minutes</Typography>
              </Box>
            </Grid>
            <Grid item>
              <Box>
                <span className={styles.times}>TOTAL</span>
                <Typography>{recipe?.prepTime + recipe?.bakeTime} minutes</Typography>
              </Box>
            </Grid>
          </Grid>
        </Box>
        <Divider></Divider>
        <Box className={styles.yieldContainer}
        >
          <span className={styles.yield}>{recipe?.yield}</span>
          <Box className={styles.buttonsContainer}>
            <Button variant="outlined" startIcon={<Save />} className={styles.button}>Save</Button>
            <Button variant="outlined" startIcon={<Print />} className={styles.button}> Print</Button>
          </Box>
        </Box>
      </Box>
      <Box>
        <img src={recipe?.image} className={styles.image} alt="" />
      </Box>

    </Box>
  )
};

