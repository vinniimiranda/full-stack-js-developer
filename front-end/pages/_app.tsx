import Breadcrumbs from '../components/Breadcrumbs'
import Navbar from '../components/Navbar'
import '../styles/globals.css'

function MyApp({ Component, pageProps }) {
  return <>
    <Navbar />
    <Breadcrumbs />
    <div className="app">
      <Component  {...pageProps} />
    </div>
  </>
}

export default MyApp
