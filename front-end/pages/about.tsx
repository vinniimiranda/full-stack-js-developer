import { Box, Typography } from "@material-ui/core";

export default function About() {
  return <Box padding="2rem">
    <Typography variant="h4">About us</Typography>
    <Typography variant="body2">Qui do qui Lorem in deserunt consectetur officia. Adipisicing exercitation nulla velit ipsum id reprehenderit nostrud. Do velit commodo anim eiusmod. Sit amet est esse commodo magna esse ex incididunt incididunt nisi sunt. Aliqua aute in duis exercitation do ex dolor.

    Magna amet aliquip incididunt minim magna nulla qui pariatur ad et elit eiusmod labore cupidatat. Anim nostrud do ut excepteur proident non cillum est commodo sit consequat ullamco ex cupidatat. Culpa minim ea cillum reprehenderit.

    Dolor consectetur occaecat laboris laboris id consequat minim ad pariatur. Consequat do sint nisi exercitation ea irure commodo. Incididunt consequat deserunt sint dolor anim nostrud. Aute et voluptate nisi amet mollit ea consectetur Lorem anim consectetur veniam. Ea do consequat ipsum nisi esse cupidatat. Est enim ut magna dolor occaecat deserunt exercitation consequat nulla. Dolor Lorem aliqua commodo ut eiusmod est nisi qui culpa laboris.

    Enim commodo irure ea esse non ipsum. Cillum laborum ut cupidatat magna labore occaecat laborum dolore occaecat quis adipisicing excepteur Lorem. Occaecat elit consectetur id dolor et. Do do est enim ad sint non duis minim est consequat. Esse ut commodo elit laboris ipsum quis ad et sit labore dolore esse.

Aliqua elit laboris qui culpa magna proident dolore ex veniam voluptate qui. Sint esse tempor ullamco enim. Officia Lorem pariatur fugiat aliqua laborum enim exercitation do. Quis aliqua aliquip veniam nostrud eu eu officia Lorem qui cillum eiusmod do velit. Do magna ullamco aliqua enim aliquip. Dolore cupidatat labore magna irure commodo consequat elit ullamco Lorem exercitation esse. Labore eiusmod labore non exercitation occaecat elit ullamco id occaecat.</Typography>
  </Box>
}
