export default [
    {
      name: 'pizza',
      link: 'recipes/pizza/',
      image: 'https://www.mapadacachaca.com.br/wp-content/uploads/2021/01/massa-de-pizza-com-cachaca.jpg',
      recipes: [
        {
          name: 'pizza',
          title: 'Pizza',
          image: 'https://www.mapadacachaca.com.br/wp-content/uploads/2021/01/massa-de-pizza-com-cachaca.jpg',
          description: 'Laborum non dolore minim aliqua qui aliqua occaecat. Lorem nisi cupidatat excepteur tempor dolore tempor veniam reprehenderit amet elit Lorem incididunt nulla velit. Aute enim enim sint exercitation',
          prepTime: 30,
          bakeTime: 10,
          yield: '8 slices of pizza'
        }
      ]
    },
    {
      name: 'hamburger',
      link: 'recipes/hamburger/',
      image: 'https://blog.eduk.com.br/wp-content/uploads/2016/08/hamburguer_shutterstock.jpg',
      recipes: [
        {
          name: 'hamburger',
          title: 'Hamburger with Cheese and Bacon',
          image: 'https://blog.eduk.com.br/wp-content/uploads/2016/08/hamburguer_shutterstock.jpg',
          description: 'Laborum non dolore minim aliqua qui aliqua occaecat. Lorem nisi cupidatat excepteur tempor dolore tempor veniam reprehenderit amet elit Lorem incididunt nulla velit. Aute enim enim sint exercitation',
          prepTime: 30,
          bakeTime: 10,
          yield: '1 big and fat Hamburger'
        }
      ]
    },
    {
      name: 'pudim',
      link: 'recipes/pudim/',
      image: 'https://s2.glbimg.com/rH585avTamAi-iQKYgOSpBz0OxE=/0x0:1280x853/924x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_e84042ef78cb4708aeebdf1c68c6cbd6/internal_photos/bs/2020/E/O/5HOokdRpWTWIAgoGdP7A/pudimmaizena.jpeg',
      recipes: [
        {
          name: 'pudim',
          title: 'Pudim',
          image: 'https://s2.glbimg.com/rH585avTamAi-iQKYgOSpBz0OxE=/0x0:1280x853/924x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_e84042ef78cb4708aeebdf1c68c6cbd6/internal_photos/bs/2020/E/O/5HOokdRpWTWIAgoGdP7A/pudimmaizena.jpeg',
          description: 'Laborum non dolore minim aliqua qui aliqua occaecat. Lorem nisi cupidatat excepteur tempor dolore tempor veniam reprehenderit amet elit Lorem incididunt nulla velit. Aute enim enim sint exercitation',
          prepTime: 30,
          bakeTime: 10,
          yield: '12 slices'
        }
      ]
    }
    
  ];
