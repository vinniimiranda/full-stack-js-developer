import ServerService from './server.service'
import axios from 'axios';
import servers from '../infra/mocks/servers';
describe('Server Service', () => {
  it('Should be defined', () => {
    expect(ServerService).toBeDefined()
  })

  it('Should return better server', async () => {
    jest.spyOn(axios, 'get').mockResolvedValue({ status: 200 });
    const server = await ServerService.findServer()
    expect(server.online).toBeTruthy()
    expect(server.priority).toBe(1)
  })

  it('Should return better server', async () => {
    const filterServers = servers.filter(({ priority }) => priority > 1)
    jest.spyOn(axios, 'get').mockResolvedValue({ status: 200 });
    ServerService.server = filterServers
    const server = await ServerService.findServer()
    expect(server.online).toBeTruthy()
    expect(server.priority).toBe(2)
  })

  it('Should return rejected Promise when timeout', async () => {
    try {
      jest.useFakeTimers();
      jest.spyOn(axios, 'get').mockResolvedValue(setTimeout(() => ({ status: 200 }), 5000));
      const server = await ServerService.findServer()
      expect(server).toThrowError()
    } catch (error) {
      expect(error).toBe("All servers offline")
    }
  })
  it('Should return rejected Promise when throw an error', async () => {
    try {
      jest.spyOn(axios, 'get').mockRejectedValue(new Error(''))
      await ServerService.findServer()
    } catch (error) {
      expect(error).toBe("All servers offline")
    }
  })
  it('Should return rejected Promise when servers were offline', async () => {
    try {
      jest.spyOn(axios, 'get').mockResolvedValue({ status: 500 });
      await ServerService.findServer()
    } catch (error) {
      expect(error).toBe("All servers offline")
    }
  })

})
