import servers from "../infra/mocks/servers"
import axios from 'axios'

interface Server {
  url: string
  priority: number
  online?: boolean
}

class ServerService {
  private _servers: Server[]

  constructor(servers: Server[]) {
    this._servers = servers
  }

  set server(servers: Server[]) {
    this._servers = servers
  }

  private async isOnline(server: Server): Promise<Server> {
    try {
      
      const { status } = await axios.get(server.url, { timeout: 5000 })
      
      const online = status >= 200 && status <= 299
      return { ...server, online }
    } catch (error) {

      return { ...server, online: false }
    }
  }

  public async findServer(): Promise<Server> {
    const serversRequest = await Promise.all(this._servers
      .map((server) => this.isOnline(server)))

    const onlineServers = serversRequest.filter(({ online }) => online)

    if (!onlineServers.length) {
      return Promise.reject('All servers offline')
    }
    const [bestServer] = onlineServers.sort((a, b) => a.priority > b.priority ? 1 : -1)
    return Promise.resolve(bestServer)
  }
}

export default new ServerService(servers)
