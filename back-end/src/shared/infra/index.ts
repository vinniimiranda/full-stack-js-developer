import ServerService from '../../modules/servers/services/server.service'

ServerService.findServer()
  .then((server) => console.log(server))
  .catch((e) => console.error(e))
